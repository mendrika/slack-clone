# slack-clone

# Database setup

Create a new postgres database named `chat`

&nbsp;
Make sur your are in nodejs-chat-api folder then run the following command `psql -h localhost -p 5432 -U postgres -d chat < chat.sql`

# Server setup

Nodejs version v14.15.5

&nbsp;
Go into `nodejs-chat-api` folder and run `npm i` to install all dependencies.

&nbsp;
You can find the database connection config file in the root of the folder `ormconfig.json`

&nbsp;
Run `npm start` to start the server application.

# Client project setup

Nodejs version v14.15.5

&nbsp;
Go into `react-chat-app` folder and run `npm i` to install dependencies.

&nbsp;
Run `npm start` to start the client application.

# Testing the app

User access

&nbsp;
email: mendrika@test.com
&nbsp;
password: mendrika123

&nbsp;
email: dave@dave.com
&nbsp;
password: dave@123
