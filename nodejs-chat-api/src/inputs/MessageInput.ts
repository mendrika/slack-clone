import { ArgsType, Field, InputType } from "type-graphql";
import { Entity } from "typeorm";
import { User } from "../models/User";

@InputType()
export class UserInput {
    @Field()
    id: string;
}
