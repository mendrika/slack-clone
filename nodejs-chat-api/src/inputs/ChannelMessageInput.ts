import { Field, InputType } from "type-graphql";

@InputType()
export class ChannelMessageInputType {
    @Field()
    messageId: string;

    @Field()
    channelId: string;
}