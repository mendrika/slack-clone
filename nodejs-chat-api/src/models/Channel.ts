import { Field, ID, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { Message } from "./Message";
import { Workspace } from "./Workspace";

@Entity()
@ObjectType()
export class Channel extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: string;

    @Field(() => String)
    @Column()
    name: string;

    @ManyToOne(type => Workspace, workspace => workspace.id) 
    @JoinColumn({name: 'workspaceId'})
    workspace: Workspace;

    @ManyToMany(type => Message, message => message.channels, {lazy: true})
    @Field(() => [Message])
    messages: Message[]

}