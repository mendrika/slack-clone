import { Field, ObjectType } from "type-graphql";

@ObjectType()
class UserObject {
    @Field()
    userId: string;

    @Field()
    email: string;
}

@ObjectType()
export class MessageObject {
    @Field()
    id: string;

    @Field()
    content: string;

    @Field()
    from: UserObject;

    @Field()
    sentAt: Date;
}

