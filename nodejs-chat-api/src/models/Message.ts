import { Field, ID, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Channel } from "./Channel";
import { User } from "./User";

@Entity()
@ObjectType()
export class Message extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: string


    @ManyToOne(type => User, user => user.id)
    @JoinColumn({name: 'from'})
    @Field(type => User)
    from: User;

    @Field(() => String)
    @Column()
    content: string;

    @Field(() => Date)
    @Column({default: () => `now()`})
    sentAt: Date;

    @ManyToMany(type => Channel, channel => channel.messages, {lazy: true, cascade: true})
    @JoinTable({
        name: 'channel_message',
        joinColumn: {
            name: 'messageId',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'channelId',
            referencedColumnName: 'id'
        }
    })
    @Field(type => [Channel])
    channels: Channel[];
}

