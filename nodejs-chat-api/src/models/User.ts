import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinColumn } from "typeorm";
import { ObjectType, Field, ID } from "type-graphql";
import { Workspace } from "./Workspace";
import { Message } from "./Message";

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: string;

  @Field(() => String)
  @Column()
  email: string;

  @Field(() => String)
  @Column()
  password: string;

  @ManyToMany(type => Workspace, workspace => workspace.users, {lazy: true})
  @Field(() => [Workspace])
  workspaces?: Workspace[];

  // @OneToMany(type => Message, message => message.from)
  // @Field(() => [Message])
  // messages: Message[]

}
