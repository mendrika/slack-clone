import { Field, ID, ObjectType } from "type-graphql";
import { BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { RelationLoader } from "typeorm/query-builder/RelationLoader";
import { Channel } from "./Channel";
import { User } from "./User";

@Entity()
@ObjectType()
export class Workspace extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id: string;

    @Field(() => String)
    @Column()
    name: string;

    @OneToMany(type => Channel, channel => channel.workspace) 
    @Field(() => [Channel])
    channels?: Channel[];

    @ManyToMany(type => User, user => user.workspaces, {lazy: true})
    @JoinTable({
        name: 'workspace_user',
        joinColumn: {
            name: 'workspaceId',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'userId',
            referencedColumnName: 'id'
        }
    })
    @Field(type => [User])
    users: User[]


}