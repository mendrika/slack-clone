import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ChannelMessage extends BaseEntity {  
    @PrimaryColumn()
    messageId: string;

    @PrimaryColumn()
    channelId: string;
}