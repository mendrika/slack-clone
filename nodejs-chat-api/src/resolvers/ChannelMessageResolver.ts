import { Arg, Mutation, Resolver } from "type-graphql";
import { ChannelMessageInputType } from "../inputs/ChannelMessageInput";
import { ChannelMessage } from "../models/ChannelMessage";

@Resolver()
export class ChannelMessageResolver {

    @Mutation(() => Boolean)
    async createChannelMessage(@Arg('data') data: ChannelMessageInputType) {
        await ChannelMessage.create(data).save()
        return true
    }
}