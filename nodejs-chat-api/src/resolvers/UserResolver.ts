import { GraphQLError, GraphQLString } from "graphql";
import {
  Arg,
  Mutation,
  PubSub,
  PubSubEngine,
  Query,
  Resolver,
  Root,
  Subscription,
} from "type-graphql";
import { CreateUserInput } from "../inputs/CreateUserInput";
import { LoginInput } from "../inputs/LoginInput";
import { UserInput } from "../inputs/MessageInput";
import { Channel } from "../models/Channel";
import { Message } from "../models/Message";
import { MessageObject } from "../models/MessageObject";
import { User } from "../models/User";

@Resolver()
export class UserResolver {
  @Query(() => [User])
  users() {
    return User.find();
  }

  @Mutation(() => User)
  async createUser(@Arg("data") data: CreateUserInput) {
    const user = User.create(data);
    await user.save();
    return user;
  }

  @Query(() => User)
  getOne(@Arg("id") id: string) {
    const user = User.findOne({ where: { id } });
    return user;
  }

  @Mutation(() => User)
  async login(@Arg("data") data: LoginInput) {
    const user = await User.findOne({ where: { email: data.email } });

    if (!user) {
      throw new Error("Adresse email n'existe pas");
    }

    if (user.password !== data.password) {
      throw new Error("Mot de passe incorrect");
    }

    console.log(user);

    return User.findOne({
      where: { id: user.id },
      relations: ["workspaces", "workspaces.channels"],
    });
  }

  @Query(() => Channel)
  getChannelMessage(@Arg("id") id: string) {
    return Channel.findOne({
      where: { id },
      relations: ["messages", "messages.from"],
    });
  }

  @Mutation(() => Message)
  async createMessage(
    @PubSub() pubSub: PubSubEngine,
    @Arg("content") content: string,
    @Arg("from") from: UserInput
  ) {
    const msg = Message.create({ content, from });

    await msg.save();
    const response = await Message.findOne({
      where: { id: msg.id },
      relations: ["from"],
    });
    await pubSub.publish("CHANNEL", {
      id: response?.id,
      content: response?.content,
      sentAt: response?.sentAt,
      from: { userId: response?.from.id, email: response?.from.email },
    });
    return response;
  }

  @Subscription({ topics: "CHANNEL" })
  channelMessage(
    @Root() { id, from, content, sentAt }: MessageObject
  ): MessageObject {
    return { id, from, content, sentAt };
  }
}
