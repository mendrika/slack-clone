import { Query, Resolver } from "type-graphql";
import { Channel } from "../models/Channel";
import { Workspace } from "../models/Workspace";

@Resolver()
export class WorkspaceResolver {
    @Query(() => [Workspace])
    async workspaces() {
        return await Workspace.find({relations: ['channels']})
    }
}