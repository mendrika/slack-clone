import "reflect-metadata";
import { createConnection } from "typeorm";
import { buildSchema } from "type-graphql";
import { UserResolver } from "./resolvers/UserResolver";
import { WorkspaceResolver } from "./resolvers/WorkspaceResolver";
import { ChannelMessageResolver } from "./resolvers/ChannelMessageResolver";
import express from "express";
import http from "http";
import { ApolloServer } from "apollo-server-express";
async function main() {
  await createConnection();

  const schema = await buildSchema({
    resolvers: [UserResolver, WorkspaceResolver, ChannelMessageResolver],
  });

  const app = express();
  const httpServer = http.createServer(app);

  const apolloServer = new ApolloServer({
    schema,
    subscriptions: {
      path: "/graphql",
      onConnect: () => {
        console.log("client connected!");
      },
    },
  });

  apolloServer.applyMiddleware({
    app,
  });
  apolloServer.installSubscriptionHandlers(httpServer);

  httpServer.listen(4000, () => {
    console.log("server is started");
  });
}

main();
