import { Button, Card, Grid, TextField } from "@mui/material";
import { useState } from "react";
import { Credentials } from "../../interfaces/credentials.interface";

interface Props {
    onSubmit: (credentials: Credentials) => void;
    error?: string;
}
const LoginComponent = (props: Props) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleSubmit = () => {
        props.onSubmit({ email, password });
    };

    return (
        <Grid container display="flex" justifyContent="center" sx={{ mt: 8 }}>
            <Card sx={{ p: 4, width: 400 }}>
                <Grid item md={12} sx={{ textAlign: "center" }}>
                    <h1>Se connecter</h1>
                </Grid>
                <Grid item md={12}>
                    <TextField label="Adresse email" size="small" fullWidth autoComplete="off" onChange={(event) => setEmail(event.target.value)} />
                </Grid>
                <Grid item md={12} sx={{ mt: 3 }}>
                    <TextField
                        type="password"
                        label="Mot de passe"
                        size="small"
                        fullWidth
                        autoComplete="off"
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </Grid>
                <Grid item md={12} sx={{ mt: 3, color: "red" }}>
                    <span>{props.error}</span>
                </Grid>
                <Grid item md={12}>
                    <Button fullWidth type="submit" variant="contained" sx={{ mt: 4 }} onClick={handleSubmit}>
                        Se connecter
                    </Button>
                </Grid>
            </Card>
        </Grid>
    );
};

export default LoginComponent;
