import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { parseResponseError } from "../../config/config";

import LoginComponent from "../components/login/login-component";
import { Credentials } from "../interfaces/credentials.interface";
import { login } from "../requests/auth-request";

export let GLOBAL_STATE: any = null;

const AuthenticationContainer = () => {
    const navigate = useNavigate();
    const [errorMsg, setErrorMsg] = useState("");

    const handleSubmit = (credentials: Credentials) => {
        login(credentials)
            .then((response: any) => {
                GLOBAL_STATE = response;
                navigate("/home");
            })
            .catch((error: any) => setErrorMsg(parseResponseError(error)));
    };

    return <LoginComponent onSubmit={handleSubmit} error={errorMsg} />;
};

export default AuthenticationContainer;
