import { mutationMethod } from "../../config/config";
import { Credentials } from "../interfaces/credentials.interface";

export const login = (data: Credentials) => {
    const request = `
    mutation {
        login(data: {email: "${data.email}", password: "${data.password}"}){
            email,
            id,
            workspaces {
              id,
              name,
              channels {
                id,
                name
              }
            }
        }
    }
    `;
    return mutationMethod(request);
};
