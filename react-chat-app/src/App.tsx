import React from "react";
import AppRoutes from "./routes/routes";
import "./App.css";
import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "./config/config";

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <div style={{ height: "100%" }}>
        <AppRoutes />
      </div>
    </ApolloProvider>
  );
}

export default App;
