import { Avatar, Grid } from "@mui/material";
import { Message } from "../conversation/interfaces/message.interface";

interface Props {
    message?: Message;
}
export const MessageComponent = (props: Props) => {
    const formatHour = (hours: string) => {
        const responses = hours.split(":");
        return `${responses[0]}h ${responses[1]}`;
    };

    return (
        <Grid container sx={{ mb: 2 }}>
            <Grid item sx={{ mr: 1 }}>
                <Avatar variant="rounded">{props.message?.from.email.split("")[0].toLocaleUpperCase()}</Avatar>
            </Grid>
            <Grid item>
                <Grid item>
                    <b>{props.message?.from.email}</b>
                    <span style={{ fontSize: "14px", color: "grey", marginLeft: "4px" }}>
                        {formatHour(new Date(props.message?.sentAt!).toLocaleTimeString())}
                    </span>
                </Grid>
                <Grid item>
                    <p>{props.message?.content}</p>
                </Grid>
            </Grid>
        </Grid>
    );
};
