import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import AuthenticationContainer from "../authentication/containers/authentication.container";
import RootContainer from "../root/containers/root-container";

const AppRoutes = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Navigate replace to="login" />} />
                <Route path="login" element={<AuthenticationContainer />} />
                <Route path="home" element={<RootContainer />} />
            </Routes>
        </BrowserRouter>
    );
};

export default AppRoutes;
