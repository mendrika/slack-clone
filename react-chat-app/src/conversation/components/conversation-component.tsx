import { Grid, TextField } from "@mui/material";
import SendIcon from "@mui/icons-material/Send";
import { MessageComponent } from "../../message/message-component";
import { Message } from "../interfaces/message.interface";
import { useState } from "react";

interface Props {
    messages: Message[];
    channel: string;
    onSendMessage: (message: string) => void;
}

const ConversationComponent = (props: Props) => {
    const [message, setMessage] = useState("");

    const handleSend = () => {
        props.onSendMessage(message);
        setMessage("");
    };

    const handleKeyDown = (event: any) => {
        if (event.key === "Enter") {
            props.onSendMessage(message);
            setMessage("");
        }
    };

    return (
        <Grid container sx={{ width: "100%", height: "100%", display: "grid", gridTemplateRows: "5% 95%" }}>
            <Grid item md={12} sx={{ borderBottom: "1px solid #E7E3E3" }}>
                <h3># {props.channel}</h3>
            </Grid>
            <Grid container sx={{ width: "100%", height: "100%" }} display="flex" alignItems="center" alignContent="end" spacing={2}>
                <Grid item md={12} sx={{ mb: 4, maxHeight: "650px", overflow: "auto" }}>
                    {props.messages.map((m, i) => (
                        <MessageComponent key={i} message={m} />
                    ))}
                </Grid>
                {props.channel && (
                    <>
                        <Grid item md={11}>
                            <TextField
                                fullWidth
                                autoComplete="off"
                                value={message}
                                onKeyDown={handleKeyDown}
                                onChange={(event) => setMessage(event.target.value)}
                            />
                        </Grid>
                        <Grid item md={1}>
                            <SendIcon sx={{ fill: "green" }} onClick={handleSend} />
                        </Grid>
                    </>
                )}
            </Grid>
        </Grid>
    );
};

export default ConversationComponent;
