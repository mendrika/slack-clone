export interface Message {
    id: string;
    content: string;
    from: User;
    sentAt?: string;
}

interface User {
    userId?: string;
    email: string;
}
