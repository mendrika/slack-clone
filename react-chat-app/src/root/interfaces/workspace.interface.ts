export interface WorkspaceInterface {
    id: string;
    name: string;
    channels: Channel[];
}

export interface Channel {
    id: string;
    name: string;
}
