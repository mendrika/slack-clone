import { Avatar, Grid, List } from "@mui/material";
import { useState } from "react";
import { Channel, WorkspaceInterface } from "../../interfaces/workspace.interface";

interface Props {
    workspaces: WorkspaceInterface[];
    onSelectChannel: (channel: Channel) => void;
}
const Workspace = (props: Props) => {
    const [selected, setSelected] = useState(props.workspaces[0]);
    return (
        <Grid container sx={{ background: "#4B1F4B", height: "100%", width: "100%", color: "white" }}>
            <Grid item md={3} sx={{ border: "1px solid #504F4F", display: "flex", alignItems: "center", flexDirection: "column", gap: "10px", p: 2 }}>
                {props.workspaces?.map((w, i) => (
                    <Avatar variant="rounded" sizes="small" key={i} onClick={() => setSelected(w)}>
                        {w.name.split("")[0]}
                    </Avatar>
                ))}
            </Grid>
            <Grid item md={9}>
                <Grid container>
                    <Grid item md={12} sx={{ textAlign: "center", border: "1px solid #504F4F" }}>
                        <h3>{selected.name}</h3>
                    </Grid>
                    <Grid item sx={{ p: 2 }}>
                        <span>Cannaux</span>
                        <List sx={{ display: "flex", flexDirection: "column", gap: "10px", pt: 2, ml: 2 }}>
                            {selected.channels.map((c, i) => (
                                <span key={i} onClick={() => props.onSelectChannel(c)}>
                                    # {c.name}
                                </span>
                            ))}
                        </List>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Workspace;
