import { Avatar, Grid } from "@mui/material";
import { StyledBadge } from "./styled-badge";

interface Props {
    name: string;
}

const HeaderComponent = (props: Props) => {
    return (
        <Grid container sx={{ p: 2, background: "#4B1F4B", height: "100%" }} alignItems="center">
            <Grid item md={12} sx={{ display: "flex", justifyContent: "flex-end" }}>
                <StyledBadge overlap="circular" anchorOrigin={{ vertical: "bottom", horizontal: "right" }} variant="dot">
                    <Avatar>{props.name.split("")[0].toLocaleUpperCase()}</Avatar>
                </StyledBadge>
            </Grid>
        </Grid>
    );
};

export default HeaderComponent;
