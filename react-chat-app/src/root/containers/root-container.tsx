import { useSubscription } from "@apollo/client";
import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { GLOBAL_STATE } from "../../authentication/containers/authentication.container";
import ConversationComponent from "../../conversation/components/conversation-component";
import { Message } from "../../conversation/interfaces/message.interface";
import HeaderComponent from "../components/header/header.component";
import Workspace from "../components/workspace/workspace";
import { Channel } from "../interfaces/workspace.interface";
import { addChannelMessage, addMessage, loadChannelMessage, subscribeForNewMessage } from "../request/root-request";

const RootContainer = () => {
    const [messages, setMessages] = useState<Message[]>([]);
    const [channel, setChannel] = useState<Channel | null>(null);
    const { data } = useSubscription(subscribeForNewMessage, {});

    useEffect(() => {
        if (data) {
            setMessages((msg) => [...msg, data?.channelMessage]);
        }
    }, [data]);

    const handleChannelChange = (c: Channel) => {
        setChannel(c);
        loadChannelMessage(c.id).then((response: any) => setMessages(response.data.getChannelMessage.messages));
    };

    const handeMessageSend = (message: string) => {
        addMessage({ from: GLOBAL_STATE.data.login.id, content: message }).then((response: any) => {
            addChannelMessage({ messageId: response.data.createMessage.id, channelId: channel?.id! });
        });
    };

    return (
        <Grid
            container
            sx={{
                height: "100%",
                display: "grid",
                gridTemplateColumns: "20% 80%",
                gridTemplateAreas: `'header header' 'side main'`,
                gridTemplateRows: "8% 92%",
            }}
        >
            <div style={{ width: "100%", height: "100%", gridArea: "header" }}>
                <HeaderComponent name={GLOBAL_STATE?.data.login.email} />
            </div>
            <Grid item sx={{ gridArea: "side", width: "100%", height: "100%" }}>
                <Workspace workspaces={GLOBAL_STATE?.data.login.workspaces} onSelectChannel={handleChannelChange} />
            </Grid>
            <Grid item sx={{ gridArea: "main", width: "100%", height: "100%", p: 2 }}>
                <ConversationComponent messages={messages} channel={channel?.name!} onSendMessage={handeMessageSend} />
            </Grid>
        </Grid>
    );
};

export default RootContainer;
