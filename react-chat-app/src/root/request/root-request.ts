import { gql } from "@apollo/client";
import { mutationMethod, queryMethod } from "../../config/config";

export const loadChannelMessage = (id: string) => {
    const request = `
    query {
        getChannelMessage(id: "${id}") {
          messages {
            from {
              email,
            },
            content,
            sentAt
          }
        }
      }
    `;

    return queryMethod(request);
};

export const addMessage = (data: { from: string; content: string }) => {
    const request = `
        mutation {
            createMessage(from: {id: "${data.from}"}, content: "${data.content}"){
                id, 
                content
            }
        }
    `;
    return mutationMethod(request);
};

export const addChannelMessage = (data: { channelId: string; messageId: string }) => {
    const request = `
      mutation {
        createChannelMessage(data : {channelId: "${data.channelId}", messageId: "${data.messageId}"})
      }
    `;
    return mutationMethod(request);
};

export const subscribeForNewMessage = gql`
    subscription {
        channelMessage {
            content
            from {
                email
                userId
            }
            id
            sentAt
        }
    }
`;
