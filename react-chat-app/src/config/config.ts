import { ApolloClient, DefaultOptions, gql, InMemoryCache } from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";

const link = new WebSocketLink({
    uri: `ws://localhost:4000/graphql`,
    options: {
        reconnect: true,
    },
});
const defaultOptions: DefaultOptions = {
    watchQuery: {
        fetchPolicy: "no-cache",
        errorPolicy: "ignore",
    },
    query: {
        fetchPolicy: "no-cache",
        errorPolicy: "all",
    },
};

export const apolloClient = new ApolloClient({
    link,
    uri: "http://localhost:4000/graphql",
    cache: new InMemoryCache(),
    defaultOptions,
});

export const queryMethod = async (object: any) => {
    return await apolloClient.query({
        query: gql`
            ${object}
        `,
    });
};

export const mutationMethod = async (object: any) => {
    return await apolloClient.mutate({
        mutation: gql`
            ${object}
        `,
    });
};

export const parseResponseError = (error: any) => {
    return JSON.parse(JSON.stringify(error))["message"];
};
